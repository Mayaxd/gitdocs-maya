= Ús bàsic del git
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

En aquesta guia es repassarà el flux de treball habitual
per treballar amb git i gitlab, amb la possibilitat de contribuir
i entregar els exercicis de manera ordenada.

== Configuració inicial del git

[IMPORTANT]
====
Tota la configuració i tasques relacionades amb el git s'han de fer amb
l'usuari de treball habitual i no amb l'usuari `root`.
====
MAYAAAAAAAAAA

- Configurem el nostre nom d'usuari i email:

[source,bash]
----
$ git config --global user.email <adreça correu>
$ git config --global user.name <nom que ha de constar als registres>
----

- Evitem que ens surti un missatge d'avís cada cop que fem
un push, indicant el sistema per defecte que ha d'utilitzar:

[source,bash]
----
$ git config --global push.default simple
----

- Configurem l'editor que volem utilitzar per editar els missatges de commit:

[source,bash]
----
$ git config --global core.editor "atom --wait"
----

Això posaria l'editor Atom com a editor per defecte. Si l'executable de
l'editor que volem no és al *path* del sistema, caldrà indicar la ruta
completa.

- Creem una clau criptogràfica per l'usuari, tal com s'explica al següent
apartat.

- Cada persona té assignat un projecte al GitLab per fer les entregues. El seu
usuari hi consta com a desenvolupador. Clonarem aquest repositori al disc local
amb `git clone`. L'ordre exacte es pot consultar al propi GitLab.

=== Autenticació amb clau criptogràfica

En comptes d'haver d'escriure una contrasenya cada vegada que necessitem
accedir al repositori, podem utilitzar una clau criptogràfica que faci la
validació automàticament des d'un ordinador que utilitzem habitualment.

Per això haurem de crear una parella de claus (pública i privada), i passar la
clau pública al servidor del Gitlab.

[IMPORTANT]
====
Cal que ningú a banda del propietari pugui accedir a la clau privada.
====

Els passos exactes per configurar una clau criptogràfica s'expliquen al
link:http://docs.gitlab.com/ce/ssh/README.html[manual del Gitlab].

En resum, els passos a seguir són:

1. Crear la clau al nostre ordinador amb:
+
[source,bash]
----
$ ssh-keygen -t rsa -C "elteu.email@exemple.com" -b 4096
----
+
En Windows farem el mateix a la línia d'ordres del git-bash.
+
Podem acceptar les opcions per defecte que ens proposa `ssh-keygen`.

2. Copiar el contingut del fitxer `id_rsa.pub` al porta-papers. Aquest fitxer
conté la clau pública, i el podem trobar dins del directori `.ssh` del nostre
directori d'usuari.
+
[TIP]
====
En GNU/Linux els directoris començats per punt (.) són ocults.
====

3. A la web del Gitlab, anem a la pestanya `SSH Keys` del nostre perfil
d'usuari, i enganxem la clau a la secció `Key`. També podem posar un títol a la
clau per tal d'identificar clarament quina clau és (si, per exemple, en tenim
de diversos ordinadors).

4. Comprovem que funciona amb l'ordre:
+
[source,bash]
----
ssh -T git@gitlab.com
----
+
Hem d'obtenir un missatge de benvinguda.

== 10 ordres bàsiques

[source,bash]
----
$ git add <fitxers/directori>
----

Afegeix fitxers per tal que el git en faci el seguiment, o marca fitxers
modificats per tal que siguin inclosos al següent commit.

Noteu que *git add* crea una còpia temporal del fitxer al repositori, de
manera que si tornem a modificar-lo podem optar per afegir de nou el fitxer o
recuperar la versió que havíem afegit prèviament.

*git add* és útil per indicar exactament quins fitxers volem incloure al
següent *commit* i per fer còpies temporals d'alguns fitxers.

[source,bash]
----
$ git commit -m "missatge"
----

Crea un nou *commit* al repositori amb tots els fitxers que s'havien afegit
anteriorment i afegeix el missatge indicat al registre. Si no especifiquem
l'opció *-m* es llançarà un editor de textos per tal d'escriure el missatge.

Si inclouem l'opció *-a* afegirà tots els fitxers modificats i farà el
*commit* en un sol pas.

L'opció *--amend* ens permet modificar l'últim *commit* si ens hem
equivocat.

[source,bash]
----
$ git status
----

Ordre bàsica per veure l'estat del directori de treball. Ens dirà quins
fitxers s'han modificat i quins s'han preparat per cometre. També ens
dóna suggeriments sobre diverses operacions habituals que podem fer.

[source,bash]
----
$ git log
----

Permet veure el registre de canvis que s'han anat incorporant al repositori.

[source,bash]
----
$ git diff
----

Serveix per visualitzar les diferències entre el directori de treball i
l'última versió que s'ha guardat al git.

Amb *--staged* veiem els canvis entre els fitxers que hem preparat amb *add*
i l'últim *commit*.

[source,bash]
----
$ git rm
----

Elimina fitxers i a més marca l'eliminació com a canvi pel següent *commit*.

[source,bash]
----
$ git mv
----

Mou un fitxer i a més marca el canvi per al següent *commit*.

[source,bash]
----
$ git checkout -- <fitxers>
----

Descarta els canvis que haguem fet als fitxers indicats, recuperant
l'última versió que estigui guardada al repositori.

És important destacar que els canvis que estem esborrant es perden
definitivament, perquè mai han arribat a guardar-se al repositori.

[source,bash]
----
$ git reset HEAD <fitxers>
----

És l'operació contrària a *git add*. Si hem afegit fitxers per accident
pel següent *commit*, els podem tornar a treure amb aquesta ordre.

[source,bash]
----
$ git help <ordre>
----

Obté la pàgina de manual corresponent a l'ordre indicada.

== Tasques habituals

=== Fer i entregar uns exercicis

Per entregar un conjunt d'exercicis és convenient treballar en una branca
específica.

- Creem una branca nova:

[source,bash]
----
$ git branch <nom nova branca>
----

Hem creat la nova branca, però encara estem treballant a master. Per canviar
a la nova branca:

[source,bash]
----
$ git checkout <nom nova branca>
----

Per poder canviar a aquesta nova branca cal abans haver guardat (commit) o
descartat tots els canvis que hi poguessin haver al nostre directori de
treball.

Podem comprovar que realment estem a la branca que volem amb:

[source,bash]
----
$ git branch
----

Un cop a la nova branca podem fer els exercicis, i fer tants commits com
necessitem. Tot això es fa en local.

Quan hem acabat, hem de pujar la nova branca al nostre repositori del GitLab:

[source,bash]
----
$ git push
----

Podem fer tants push com necessitem. El primer push que fem per una branca nova
ha d'indicar que volem crear la mateixa branca al repositori del Gitlab. Això
ho indiquem al fer el push amb:

[source,bash]
----
$ git push --set-upstream origin <nom branca>
----

Per tal d'entregar definitivament els exercicis anem a la web del GitLab i
obrim un nou *merge request* de la nostra branca nova cap a la branca master.

Un cop el professor hagi acceptat el merge request, els exercicis entregats
passaran a la branca master i s'eliminarà la branca utilitzada al Gitlab.

A partir d'aquí, es pot actualitzar el respositori local amb:

[source,bash]
----
$ git pull
----

i si volem podem desfer-nos ja de la branca local:

[source,bash]
----
$ git branch -D <nom branca>
----

=== Canviar de branca amb feina a mig fer

Sovint voldrem canviar de branca de treball perquè estem treballant en un
grup d'exercicis diferent a diferents branques.

Si tots els canvis que hem fet s'han guardat al git amb commit no hi ha cap
problema. Però de vegades tenim canvis a mig fer que no volem perdre, però que
tampoc volem guardar encara com un commit al repositori.

En aquests casos és molt útil l'ordre:

[source,bash]
----
$ git stash
----

Aquesta ordre guarda una còpia de tot el que tenim al directori de treball.
A partir d'aquí podem canviar de branca sense por a perdre aquestes dades.

Quan més tard tornem a aquesta branca de treball podem recuperar el que
estàvem fent.

Podem veure totes les captures que tenim guardades:

[source,bash]
----
$ git stash list
----

Cada captura tindrà un número, que podem recuperar amb:

[source,bash]
----
$ git stash apply stash@{núm}
----

O amb:

[source,bash]
----
$ git stash apply
----

si volem recuperar l'última captura que hem fet.

Un cop recuperades aquestes dades, segurament voldrem esborrar la còpia
amb:

[source,bash]
----
$ git stash drop stash@{núm}
----

Finalment, si el que volem és recuperar l'última captura que hem fet i
després esborrar-la, ho podem fer tot amb una sola ordre:

[source,bash]
----
$ git stash pop
----

=== Resoldre un conflicte

Quan unim dues branques amb un *merge* pot passar que hi hagi una part
d'algun fitxer que s'hagi modificat a les dues branques. Aleshores el git
no sap com ha d'unir les dues versions i es produeix un conflicte.

A les següent seccions s'explica com es poden resoldre aquests conflictes,
però si ens atavalem i volem anul·lar el *merge* sempre ho podem fer amb:

[source,bash]
----
$ git merge --abort
----

Cal tenir en compte que abortar un *merge* pot afectar als fitxers del
nostre directori de treball que no hagin estat guardats amb un *commit* o un
*stash*. Per això, sempre és recomanable **tenir el directori de treball
sense canvis per desar** abans de provar un *merge*.

==== Fitxers de text

Si el fitxer en conflicte és un fitxer de text, el git ens haurà afegit
uns indicadors al propi fitxer, i entre aquests indicadors les dues
versions en conflicte:

[source]
----
<<<<<<< branca_actual:fitxer
Part en conflicte del fitxer a la branca actual
=======
Part en conflicte del fitxer a la branca que s'està integrant
>>>>>>> branca_a_integrar:fitxer
----

A partir d'aquesta informació podem quedar-nos amb les parts que volem i
eliminar les marques que ha afegit el git.

Un cop hem resol el conflicte li indiquem al git fent un *git add* del o
dels fitxers que tenien conflictes.

Finalment, acabarem el *merge* amb un *git commit*.

==== Fitxers binaris

En el cas de fitxers binaris, com imatges, no podem barrejar les dues
versions: o ens quedem amb la nostra versió de la branca actual (*ours*) o
amb la versió que ve de l'altra branca (*theirs*).

Si volem la versió de la nostra branca actual fem:

[source,bash]
----
$ git checkout --ours -- fitxer_en_conflicte
----

I si volem la versió de la branca que estem integrant:

[source,bash]
----
$ git checkout --theirs -- fitxer_en_conflicte
----

A partir d'aquí procedim com en el cas anterior: afegint el fitxer final
amb un *git add* i acabant l'operació de *merge* amb un *git commit*.

=== Contribuir a millorar els apunts

Per modificar els apunts del Gitlab primer de tot necessitem fer un fork del
repositori original dins del propi Gitlab.

Si el canvi és petit, el podem fer a la mateixa web del Gitlab i crear
directament el merge request cap al projecte original.

Si volem fer modificacions més grans, caldrà treballar en local. Per això,
primer farem un `git clone` de la nostra còpia dels apunts.

[WARNING]
====
Hem de clonar el nostre fork del repositori, no el repositori original.
====

[source,bash]
----
$ git clone https://gitlab.com/<usuari>/DAM-2n-POO-i-acces-a-dades.git
----

- Afegim el repositori original com un remot més del nostre
projecte:

[source,bash]
----
$ git remote add upstream https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades.git
----

Per veure la configuració actual dels repositoris remots
podem utilitzar:

[source,bash]
----
$ git remote -v
----

==== Actualitzar la nostra còpia amb els canvis més recents

Això només cal fer-ho si ja fa un temps que vam crear la nostra copia dels
apunts i en aquest temps hi ha hagut canvis.

- Baixem tota la informació nova del repositori:

[source,bash]
----
$ git fetch upstream
----

El *fetch* crea una nova branca anomenada
*remotes/upstream/master* al nostre repositori local, amb el
contingut del repositori remot que hem anomenat *upstream*
durant la configuració inicial.

Per veure totes les branques, també les remotes, podem fer:

[source,bash]
----
$ git branch -a
----

- Unim la branca upstream/master que hem obtingut a la
nostra branca master:

[source,bash]
----
$ git merge upstream/master
----

- Si hem fet canvis directament a la web del GitLab, caldrà que
els baixem i els integrem també:

[source,bash]
----
$ git pull
----

L'ordre *git pull* és una forma ràpida de fer un *fetch* i un
*merge*. Noteu que amb aquesta instrucció estem fent gairebé
el mateix que abans, però amb el nostre propi repositori a GitLab.

Un cop integrat tot en el nostre repositori local, ja ho
podem pujar tot de nou al GitLab:

[source,bash]
----
$ git push
----

A més d'això, probablement voldrem passar els canvis d'aquesta branca nova
a la nostra branca master, i evitar així tenir múltiples branques diferents.

Per ajuntar la nova branca a la branca master ens canviem primer a la branca
master i després fem un merge de la branca nova:

[source,bash]
----
$ git checkout master
$ git merge <nom nova branca>
----

Com sempre, podem pujar aquests canvis al GitLab amb:

[source,bash]
----
$ git push
----

i si volem podem desfer-nos ja d'aquesta branca local:

[source,bash]
----
$ git branch -D <nom nova branca>
----

==== Modificar els apunts i crear el merge request

Un cop configurada la nostra còpia local ja podem començar a treballar. Ho
farem de la mateixa manera que utilitzem per entregar exercicis: podem fer tants
commit i push com necessitem, però és millor si hem creat primer una branca
específica pel canvi que volem fer.

Un cop pujada la branca al nostre fork, crearem el merge request. Això funciona
igual que quan entreguem exercicis, amb l'única diferència que hem d'indicar
que volem fer el merge request contra el projecte original.
